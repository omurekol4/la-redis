﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RedisForm
{
    public partial class RedisForm : Form
    {
        PersonRepository psrepo { get; set; }
        DepartmentRepository dprepo { get; set; }
        bool isMemcached =false;
        public RedisForm()
        {
            InitializeComponent();
             psrepo = new PersonRepository();
             dprepo = new DepartmentRepository();
        }

        private void btn_kaydet_Click(object sender, EventArgs e)
        {
            Person person = new Person();
            
            List<Person> persons = new List<Person>();
        
            person.Name = txt_name.Text;

            person.Level = Convert.ToInt32(txt_level.Text);

            person.Department = dprepo.create(cmb_department.Text,"Yet Another Department");
             
            persons.Add(person);


            var res = psrepo.insert(persons);
            isMemcached = true;
            foreach (var item in res)
            {

                MessageBox.Show(item.PersonId+" , "+item.Name.ToString()+" , "+item.Level+" , "+item.Department.Name);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (isMemcached)
            {
                psrepo.diskeyaz();
                MessageBox.Show("Yazıldı!");
                isMemcached = false;
            }
            else
            {
                MessageBox.Show("Önce Cache de yazınız!");
            }
            
        }
    }
}
