﻿using ServiceStack.Redis;
using ServiceStack.Redis.Generic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedisForm
{
    public class PersonRepository : BaseRepository
    {
        IRedisTypedClient<Person> personStore { get; set; }
        public PersonRepository():base(){

           personStore = client.As<Person>();
        }    

      
        public List<Person> insert(List<Person> persons) {
            foreach (var item in persons)
            {
                item.PersonId = personStore.GetNextSequence();
            }
            personStore.StoreAll(persons);
            return persons;
        }

        public void diskeyaz() {
            personStore.SaveAsync();
        }
        
    }
}
